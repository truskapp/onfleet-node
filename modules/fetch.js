'use strict';

var _ = require('lodash');
require('es6-promise').polyfill();
require('isomorphic-fetch');
var utils = require('./utils');

module.exports = function(params, callback){
    var query = _.extend({}, params.query);
    var queryString =  !_.isEmpty(query) ? ('?' + utils.serialize(query)) : '';
    var url = (params.protocol || 'http') + '://' + (params.key ? (params.key + '@') : '') + params.host + (params.port  ? (':' + params.port) : '') + (params.basePath || '') + (params.version ? '/' + params.version : '') + params.path + queryString;
    return fetch(url, {
        method: params.method,
        headers: params.headers || {'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: params.method && _.contains(['post', 'put'], params.method.toLowerCase()) ? JSON.stringify(params.body || {}) : undefined
    })
    .then(function(response) {
        if (response.status >= 200 && response.status < 300)
            return _.extend(response.json(), {code : response.status});
        return response.json().then(function(error) {
            throw _.extend(error, {code : response.status});
        });
    });
};
