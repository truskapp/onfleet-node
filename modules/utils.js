'use strict';

var _ = require('lodash');

var utils = {};

utils.serialize = function(obj) {
    var str = [];
    for (var p in obj)
        if (obj.hasOwnProperty(p)) {
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        }
    return str.join('&');
};

utils.matchPath = function(path, idArray){
    var count = 0;
    return _.map(path.split('/'), function(p){
        return (p.charAt(0) == ':') ? ((count < idArray.length) ? idArray[count++] : 'NO_KEY') : p;
    }).join('/');
};

module.exports = utils;
