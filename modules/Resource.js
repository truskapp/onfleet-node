var _ = require('lodash');
var fetch = require('./fetch');
var utils = require('./utils');

function Resource(api) {
    _.extend(this, api);
};

Resource.prototype.endpoints = function(endpoints){
    var that = this;
    _.each(_.keys(endpoints), function(key){
        that[key] = function(){
            var args = Array.prototype.slice.call(arguments, 0);
            var callback = (_.isFunction(_.last(args)) ? _.last(args) : undefined);
            args = callback ? args.slice(0, -1) : args;
            var body = (_.isObject(_.last(args)) ? _.last(args) : undefined);
            args = body ? args.slice(0, -1) : args;
            var path = utils.matchPath(endpoints[key].path, args);
            var call = fetch(_.extend({}, that._api, {path : path, method : endpoints[key].method}, {body : body}));
            return callback ? call.then(function(data){callback(null, data)}).catch(function(e){callback(e)}) : call;
        };
    });
};

module.exports = Resource;
