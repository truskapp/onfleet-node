'use strict';

var Resource = require('../modules/Resource');

module.exports = function(api){
    var resource = new Resource(api);
    resource.endpoints({
        create: {
            path: '/destinations',
            method: 'POST'
        },
        retrieve: {
            path: '/destinations/:destinationId',
            method: 'GET'
        }
    });
    return resource;
};
