'use strict';

var Resource = require('../modules/Resource');

module.exports = function(api){
    var resource = new Resource(api);
    resource.endpoints({
        create: {
            path: '/tasks',
            method: 'POST'
        },
        list: {
            path: '/tasks',
            method: 'GET'
        },
        retrieve: {
            path: '/tasks/:taskId',
            method: 'GET'
        },
        update: {
            path: '/tasks/:taskId',
            method: 'PUT'
        },
        complete: {
            path: '/tasks/:taskId/complete',
            method: 'POST'
        },
        delete: {
            path: '/tasks/:taskId',
            method: 'DELETE'
        }
    });
    return resource;
};
