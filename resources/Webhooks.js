'use strict';

var Resource = require('../modules/Resource');

module.exports = function(api){
    var resource = new Resource(api);
    resource.endpoints({
        create: {
            path: '/webhooks',
            method: 'POST'
        },
        list: {
            path: '/webhooks',
            method: 'GET'
        },
        delete: {
            path: '/webhooks/:webhookId',
            method: 'DELETE'
        }
    });
    return resource;
};
