'use strict';

Onfleet.DEFAULT_HOST = 'onfleet.com';
Onfleet.DEFAULT_PORT = null;
Onfleet.DEFAULT_PROTOCOL = 'https';
Onfleet.DEFAULT_BASE_PATH = '/api';
Onfleet.DEFAULT_API_VERSION = 'v2';

// Use node's default timeout:
Onfleet.DEFAULT_TIMEOUT = require('http').createServer().timeout;
Onfleet.PACKAGE_VERSION = require('./package.json').version;

var resources = {
    Administrators : require('./resources/Administrators'),
    Destinations : require('./resources/Destinations'),
    Organizations : require('./resources/Organizations'),
    Recipients : require('./resources/Recipients'),
    Tasks : require('./resources/Tasks'),
    Teams : require('./resources/Teams'),
    Workers : require('./resources/Workers'),
    Webhooks : require('./resources/Webhooks')
};

Onfleet.resources = resources;

function Onfleet(key, version) {
    if (!(this instanceof Onfleet)) {
        return new Onfleet(key, version);
    }

    this._api = {
        key : null,
        host: Onfleet.DEFAULT_HOST,
        port: Onfleet.DEFAULT_PORT,
        protocol : Onfleet.DEFAULT_PROTOCOL,
        basePath: Onfleet.DEFAULT_BASE_PATH,
        version: Onfleet.DEFAULT_API_VERSION,
        timeout: Onfleet.DEFAULT_TIMEOUT
    };

    this.setApiKey(key);
    this.setApiVersion(version);
    this._prepResources();
}

Onfleet.prototype = {
    setApiVersion: function(version) {
        if(version)
            this._setApiField('version', version);
    },
    setApiKey: function(key) {
        if(key)
            this._setApiField('key', key);
    },
    _setApiField: function(key, value) {
        this._api[key] = value;
    },
    _prepResources: function() {
        for (var name in resources)
            this[name[0].toLowerCase() + name.substring(1)] = new resources[name](this);
    }
};

module.exports = Onfleet;
